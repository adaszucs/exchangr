package com.revolut.adamszucs.exchangr.ui.main.adapter

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.revolut.adamszucs.exchangr.BR
import com.revolut.adamszucs.exchangr.R
import com.revolut.adamszucs.exchangr.databinding.ListItemExchangeRateBinding
import com.revolut.adamszucs.exchangr.databinding.ListItemExchangeRateFirstBinding
import com.revolut.adamszucs.exchangr.exchangerates.model.CurrentCurrencyObservable

class ExchangeRateAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var currentCurrencyObservable: CurrentCurrencyObservable? = null

    private var adapterListener: ExchangeRateAdapterListener? = null

    private var itemViewModels: List<ExchangeRatesListItemViewModel> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            ITEM_FIRST -> FirstItemViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.list_item_exchange_rate_first,
                    parent,
                    false
                )
            )
            else -> RestItemsViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.list_item_exchange_rate,
                    parent,
                    false
                )
            )
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is FirstItemViewHolder -> holder.bind()
            is RestItemsViewHolder -> holder.bind()
        }
    }

    override fun getItemId(position: Int): Long =
        itemViewModels[position].country.hashCode().toLong()

    override fun getItemCount(): Int = itemViewModels.size

    override fun getItemViewType(position: Int): Int =
        when (position) {
            0 -> ITEM_FIRST
            else -> ITEM_REST
        }

    fun setAdapterListener(listener: ExchangeRateAdapterListener) {
        adapterListener = listener
    }

    fun setItemViewModels(items: List<ExchangeRatesListItemViewModel>) {
        itemViewModels = items
        notifyDataSetChanged()
    }

    fun setCurrentCurrencyObservable(observable: CurrentCurrencyObservable?) {
        currentCurrencyObservable = observable
    }

    inner class RestItemsViewHolder(
        private val binding: ListItemExchangeRateBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        private val editText =
            itemView.findViewById<EditText>(R.id.currency_edit_text)

        fun bind() {
            binding.setVariable(BR.exchangeRate, itemViewModels[adapterPosition])
            binding.executePendingBindings()

            itemView.setOnClickListener {
                moveItemViewModelToFront()
            }
            editText.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    moveItemViewModelToFront()
                }
            }
        }

        private fun moveItemViewModelToFront() {
            val clickedItem = itemViewModels[adapterPosition]
            val restOfItems = itemViewModels.filter { item -> item != clickedItem }

            itemViewModels = listOf(clickedItem) + restOfItems

            currentCurrencyObservable?.currentCurrencyValue =
                clickedItem.currencyValue.get().toString()
            adapterListener?.onCurrencyRowSelected(clickedItem.country)
            notifyItemRangeChanged(0, adapterPosition + 1)
            notifyItemMoved(adapterPosition, 0)
        }
    }

    inner class FirstItemViewHolder(
        private val binding: ListItemExchangeRateFirstBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        private val editText =
            itemView.findViewById<EditText>(R.id.list_item_exchange_rate_current_rate_first)

        fun bind() {
            binding.setVariable(BR.exchangeRate, itemViewModels[adapterPosition])
            binding.setVariable(
                BR.currentCurrencyValue,
                currentCurrencyObservable?.currentCurrencyValue
            )
            binding.executePendingBindings()

            val textWatcher = object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                    // nothing to do
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    // nothing to do
                }

                override fun afterTextChanged(s: Editable?) {
                    currentCurrencyObservable?.currentCurrencyValue = s?.toString()
                        ?.let { actual ->
                            if (actual == "") {
                                "0.0"
                            } else {
                                actual
                            }
                        } ?: "0.0"
                }
            }

            editText.requestFocus()
            editText.removeTextChangedListener(textWatcher)
            editText.addTextChangedListener(textWatcher)
        }
    }

    interface ExchangeRateAdapterListener {
        fun onCurrencyRowSelected(currency: String)
    }

    companion object {
        const val ITEM_FIRST = 0
        const val ITEM_REST = 1
    }
}