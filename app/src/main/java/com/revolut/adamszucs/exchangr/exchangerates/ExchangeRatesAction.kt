package com.revolut.adamszucs.exchangr.exchangerates

import com.revolut.adamszucs.exchangr.exchangerates.model.ExchangeRatesResponse
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ExchangeRatesAction {

    private val exchangeRatesRetrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val exchangeRatesApi: ExchangeApi = exchangeRatesRetrofit
        .create(ExchangeApi::class.java)

    fun fetchExchangeRates(base: String = "EUR"): Single<ExchangeRatesResponse> = exchangeRatesApi
        .getExchangeRates(base)
        .subscribeOn(Schedulers.computation())

    companion object {
        const val BASE_URL = "https://hiring.revolut.codes/api/android/"
    }
}