package com.revolut.adamszucs.exchangr.exchangerates.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.revolut.adamszucs.exchangr.BR

class CurrentCurrencyObservable : BaseObservable() {

    @get:Bindable
    var currentCurrencyValue: String = "100.0"
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.currentCurrencyValue)
            }
        }
}