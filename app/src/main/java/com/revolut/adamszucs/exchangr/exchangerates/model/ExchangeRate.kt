package com.revolut.adamszucs.exchangr.exchangerates.model

data class ExchangeRate(
    val country: String,
    val rate: Double
)