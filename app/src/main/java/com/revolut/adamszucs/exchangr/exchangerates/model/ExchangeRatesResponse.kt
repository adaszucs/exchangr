package com.revolut.adamszucs.exchangr.exchangerates.model

data class ExchangeRatesResponse(
    val baseCurrency: String,
    val rates: Map<String, Double>
)
