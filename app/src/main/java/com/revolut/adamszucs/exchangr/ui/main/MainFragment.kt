package com.revolut.adamszucs.exchangr.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.revolut.adamszucs.exchangr.BR
import com.revolut.adamszucs.exchangr.R
import com.revolut.adamszucs.exchangr.databinding.MainFragmentBinding
import com.revolut.adamszucs.exchangr.ui.main.adapter.ExchangeRateAdapter

class MainFragment : Fragment(), ExchangeRateAdapter.ExchangeRateAdapterListener {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.main_fragment,
            container,
            false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        binding.setVariable(BR.vm, viewModel)
        binding.setVariable(BR.adapterListener, this)
        binding.setVariable(
            BR.baseCurrencyValue,
            viewModel.currentCurrencyObservable
        )

        binding.executePendingBindings()
    }

    override fun onCurrencyRowSelected(currency: String) {
        viewModel.setBaseCurrency(currency)
    }
}