package com.revolut.adamszucs.exchangr.ui.main.adapter

import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.recyclerview.widget.RecyclerView
import com.revolut.adamszucs.exchangr.R
import com.revolut.adamszucs.exchangr.exchangerates.model.CurrentCurrencyObservable
import com.squareup.picasso.Picasso

@BindingAdapter("items", "adapterListener", "baseCurrencyValue", requireAll = true)
fun bindItemViewModelsToRecyclerView(
    view: RecyclerView,
    items: List<ExchangeRatesListItemViewModel>?,
    adapterListener: ExchangeRateAdapter.ExchangeRateAdapterListener?,
    baseCurrencyValue: CurrentCurrencyObservable?
) {
    items ?: return
    adapterListener ?: return

    var adapter: ExchangeRateAdapter? = view.adapter as? ExchangeRateAdapter

    if (adapter == null) {
        adapter = ExchangeRateAdapter()

        adapter.registerAdapterDataObserver(object :
            RecyclerView.AdapterDataObserver() {
            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                if (fromPosition == 0 || toPosition == 0) {
                    view.scrollToPosition(0)
                }
            }
        })
        adapter.setAdapterListener(adapterListener)
        adapter.setCurrentCurrencyObservable(baseCurrencyValue)

        view.adapter = adapter
    }

    adapter.setItemViewModels(items)
}

@BindingAdapter("currencyValueRest")
fun bindCurrencyValueRestToEditText(
    view: EditText,
    currencyValue: Double?
) {
    currencyValue ?: return

    view.setText(String.format("%.2f", currencyValue))
}

@BindingAdapter("currencyValue", "currencyValueAttrChanged", requireAll = false)
fun bindCurrencyValueToEditText(
    view: EditText,
    currencyValue: String?,
    listener: InverseBindingListener?
) {
    currencyValue ?: return

    val previousValue = view.text.toString()
    view.setText(String.format("%.2f", currencyValue.toDoubleOrNull()))

    if (previousValue.toDoubleOrNull() != currencyValue.toDoubleOrNull()) {
        listener?.onChange()
    }
}

@InverseBindingAdapter(attribute = "currencyValue")
fun getCurrencyValueFromEditText(
    view: EditText
): String = view.text.toString()

@BindingAdapter("flag")
fun setFlag(
    view: ImageView,
    country: String?
) {
    country ?: return

    Picasso.Builder(view.context)
        .build()
        .load("https://www.countryflags.io/${country.substring(0, 2)}/flat/64.png")
        .error(R.drawable.euro_flag)
        .into(view)
}