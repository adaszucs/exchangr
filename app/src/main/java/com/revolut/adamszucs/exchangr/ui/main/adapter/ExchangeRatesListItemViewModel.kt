package com.revolut.adamszucs.exchangr.ui.main.adapter

import androidx.databinding.ObservableField
import io.reactivex.rxjava3.core.Flowable

class ExchangeRatesListItemViewModel(
    val country: String,
    exchangeRateFlowable: Flowable<Double>
) {

    var currencyValue = ObservableField<Double>()

    init {
        exchangeRateFlowable.subscribe { currencyValue.set(it) }
    }
}
