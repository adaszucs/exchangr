package com.revolut.adamszucs.exchangr.ui.main

import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.jakewharton.rx3.replayingShare
import com.revolut.adamszucs.exchangr.BR
import com.revolut.adamszucs.exchangr.exchangerates.ExchangeRatesAction
import com.revolut.adamszucs.exchangr.exchangerates.model.CurrentCurrencyObservable
import com.revolut.adamszucs.exchangr.exchangerates.model.ExchangeRate
import com.revolut.adamszucs.exchangr.ui.main.adapter.ExchangeRatesListItemViewModel
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.processors.BehaviorProcessor
import java.util.concurrent.TimeUnit

class MainViewModel : ViewModel() {
    val currentCurrencyObservable = CurrentCurrencyObservable()

    private val currentCurrencyValueProcessor = BehaviorProcessor.createDefault(
        currentCurrencyObservable.currentCurrencyValue.toDoubleOrNull()
    )

    private val baseCurrency = ObservableField(COUNTRY_EUR)

    private val disposables = CompositeDisposable()

    private val exchangeRatesAction = ExchangeRatesAction()

    private val exchangeRatesFlowable = Flowable
        .defer { Flowable.just(baseCurrency) }
        .flatMap { country ->
            generateRequestForExchangeRates(country.get() ?: COUNTRY_EUR)
        }
        .repeatWhen { flowable -> flowable.delay(1, TimeUnit.SECONDS) }
        .retry()
        .replayingShare()

    val adapterItems = ObservableField<List<ExchangeRatesListItemViewModel>>()

    fun setBaseCurrency(currency: String) {
        baseCurrency.set(currency)
    }

    init {
        getAdapterItemViewModels()
        currentCurrencyObservable.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (propertyId == BR.currentCurrencyValue) {
                    currentCurrencyValueProcessor.onNext(
                        (sender as CurrentCurrencyObservable).currentCurrencyValue.toDouble()
                    )
                }
            }

        })
    }

    private fun generateRequestForExchangeRates(
        country: String
    ): Flowable<List<ExchangeRate>> = exchangeRatesAction
        .fetchExchangeRates(country)
        .map { list ->
            list.rates.map { ExchangeRate(it.key, it.value) }
        }
        .toFlowable()

    private fun getAdapterItemViewModels() {
        exchangeRatesFlowable
            .take(1)
            .map { rates ->
                val viewModels = rates
                    .map { item ->
                        createExchangeRateItemViewModel(item.country)
                    }.toMutableList()

                viewModels.add(
                    0,
                    createExchangeRateItemViewModel(COUNTRY_EUR)
                )

                viewModels
            }
            .subscribe { list -> adapterItems.set(list) }
            .addTo(disposables)
    }

    private fun createExchangeRateItemViewModel(country: String) =
        ExchangeRatesListItemViewModel(
            country,
            Flowable
                .combineLatest(
                    exchangeRatesFlowable.filter { rateList ->
                        rateList.any { rateItem -> rateItem.country == country }
                    },
                    currentCurrencyValueProcessor
                ) { exchangeRates, baseValue ->
                    (exchangeRates.firstOrNull { it.country == country }?.rate
                        ?: 0.0) * (baseValue ?: 0.0)
                }
        )

    override fun onCleared() {
        disposables.clear()

        super.onCleared()
    }

    companion object {
        const val COUNTRY_EUR = "EUR"
    }
}
