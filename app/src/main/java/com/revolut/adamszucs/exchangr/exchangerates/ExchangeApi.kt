package com.revolut.adamszucs.exchangr.exchangerates

import com.revolut.adamszucs.exchangr.exchangerates.model.ExchangeRatesResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ExchangeApi {

    @GET("latest")
    fun getExchangeRates(@Query("base") base: String): Single<ExchangeRatesResponse>
}